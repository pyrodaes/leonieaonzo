---
title: Melancholy light
date: 16/07/2017
image: 041.jpg
tags: painting
---

The courtyard of my apartment in Genoa. The only place to find silence in a city.
Watercolors.
