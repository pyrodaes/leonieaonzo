---
title: The Siren and the Fisherman
date: 20/11/2017
image: 052.jpg
tags: painting
---

First of three illustrations commissioned by E-Motion (Genova) for the San Lorenzo Yacht anniversary.
Digital painting (Krita).
