---
layout: post
title: "Photo Story"
date: 13/09/2017
categories: project
post-header: 2017-09-13.jpg
info: "A story inside a picture"
---

A few days ago, a friend of mine told me about an assignment common in his university (Urbino's ISIA): choose a photograph, isolate parts of it and create a narrative independent from the original picture. I though it would be fun to try and do the same, starting form the same photograph he chose: one of Herbert List's photographies from Naples.

<img class="post-img" src="{{ site.baseurl }}/img/posts/2017-09-13-01.jpg" />

<h2>The Story</h2>
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/01.jpg" />
The gait of the old grew unexpectedly fast.
<br style="clear:both">
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/02.jpg" />
For the little girl who followed her in the shadows, it was more and more difficult to keep up the pace without being noticed.
<br style="clear:both">
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/03.jpg" />
Especially since they were moving further and further away from the center of the town.
<br style="clear:both">
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/04.jpg" />
What to do? The night was falling. Was that mystery really worth the scolding that would await her at home?
<br style="clear:both">
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/05.jpg" />
Curiosity prevaled. She went on.
<br style="clear:both">
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/06.jpg" />
The old woman entered a silent alley and began to descend some steep stairs, perhaps an ancient passage through the city walls.
<br style="clear:both">
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/07.jpg" />
When the sky vaulted over them again, the full moon almost dazzled the little girl.
<br style="clear:both">
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/08.jpg" />
The old suddenly stopped.
<br style="clear:both">
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/09.jpg" />
She fumbled in her bag, without suspecting being observed ...
<br style="clear:both">
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/10.jpg" />
... and she brought out an elegant pair of dance shoes, which she wore with ease.
<br style="clear:both">
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/11.jpg" />
She made a pirouette and around her, out of nowhere, twirled a silk fabric. Her legs were suddenly released from the weight of the years.
<br style="clear:both">
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/12.jpg" />
The little girl could not believe her eyes. It was true then! The story that her grandfather used to tell her as a joke...
<br style="clear:both">
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/13.jpg" />
Before her amazement could end, a new figure entered the scene: it was a man. He, too, was elegantly dressed.
<br style="clear:both">
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/14.jpg" />
The two of them, wearing macabre masks, danced in spite of death.
<br style="clear:both">
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/15.jpg" />
An accordion music was grafted on their steps, first distant, then slowly approaching, together with its player.
<br style="clear:both">
<img class="post-img-side" src="{{ site.baseurl }}/img/posts/2017-09-13/16.jpg" />
The little girl woke up in her bed with a start, disoriented. In front of her she could still see the dancers in their beautiful dresses, the milky reflections on the cheekbones of their masks, their perfect movements. She could not recreate that music in her mind, that was yet so penetrating, but its sound still made her skin vibrate: she had not dreamed.
